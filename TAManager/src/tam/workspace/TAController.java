package tam.workspace;

import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.style.TAStyle;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    private boolean validateEmail(String email){
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN =
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
	return matcher.matches();
    }
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));            
        }
        else if(!validateEmail(email)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));          
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name,email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            data.addTA(name,email);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
        }
    }
    public void handleUpdateTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        TableView<TeachingAssistant> taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));            
        }
        else if(!validateEmail(email)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));          
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (!data.validChange(ta,name,email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        else {
            // ADD THE NEW TA TO THE DATA
            data.changeTA(ta,name,email);
            workspace.getTATable().refresh();
            
        }
    }
public void handleTASelectionChange(){
    TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
    TableView<TeachingAssistant> taTable = workspace.getTATable();
    if(!taTable.getSelectionModel().isEmpty()){
        TeachingAssistant selected = (TeachingAssistant) taTable.getSelectionModel().getSelectedItem();
        workspace.getNameTextField().setText(selected.getName());
        workspace.getEmailTextField().setText(selected.getEmail());
        workspace.updateTAMode();
    }
}
public void handleTAClear(){
    TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
    TableView<TeachingAssistant> taTable = workspace.getTATable();
    workspace.addTAMode();
    workspace.getNameTextField().requestFocus(); 
}
    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        if(taTable.getSelectionModel().isEmpty())
            return;
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        TAData data = (TAData)app.getDataComponent();
        String cellKey = pane.getId();
        
        // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
        data.toggleTAOfficeHours(cellKey, taName);
    }
    public void handleCellEnter(Pane pane, String key){
        TAData data = (TAData) app.getDataComponent();
        TAStyle style = (TAStyle) app.getStyleComponent();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        HashMap<String,Pane> tacellpanes = workspace.getOfficeHoursGridTACellPanes();
        HashMap<String,Pane> timeCellPanes = workspace.getOfficeHoursGridTimeCellPanes();
        HashMap<String,Pane> dayHeaderPanes = workspace.getOfficeHoursGridDayHeaderPanes();
        String[] index = key.split("_");
        
        //highlight row and column of cell panes
        for(int i = 0; i < Integer.parseInt(index[1]); i++){
            if(i == 0)
                style.styleChange(index[0] + "_" + i,dayHeaderPanes,TAStyle.CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE_HL);
            else
                style.styleChange(index[0] + "_" + i,tacellpanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_HL2);
        }
        for(int i = 0; i < Integer.parseInt(index[0]); i++){
            if(i < 2)
                style.styleChange(i + "_" + index[1],timeCellPanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE_HL);
            else
                style.styleChange(i + "_" + index[1],tacellpanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_HL2);
        }
        
        //highlight self
        style.styleChange(key,tacellpanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_HL1);
        
        //highlight time

    }
    public void handleCellExit(Pane pane, String key){
        TAData data = (TAData) app.getDataComponent();
        TAStyle style = (TAStyle) app.getStyleComponent();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        HashMap<String,Pane> tacellpanes = workspace.getOfficeHoursGridTACellPanes();
        HashMap<String,Pane> timeCellPanes = workspace.getOfficeHoursGridTimeCellPanes();
        HashMap<String,Pane> dayHeaderPanes = workspace.getOfficeHoursGridDayHeaderPanes();
        String[] index = key.split("_");
        
        //highlight row and column of cell panes
        for(int i = 0; i <= Integer.parseInt(index[1]); i++){
            if(i == 0)
                style.styleChange(index[0] + "_" + i,dayHeaderPanes,TAStyle.CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
            else
                style.styleChange(index[0] + "_" + i,tacellpanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
        for(int i = 0; i <= Integer.parseInt(index[0]); i++){
            if(i < 2)
                style.styleChange(i + "_" + index[1],timeCellPanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
            else
                style.styleChange(i + "_" + index[1],tacellpanes,TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

    }
    public void handleDeleteTA(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        if(taTable.getSelectionModel().isEmpty())
            return;
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        TAData data = (TAData)app.getDataComponent();
        data.deleteTA(ta);
        
    }
}